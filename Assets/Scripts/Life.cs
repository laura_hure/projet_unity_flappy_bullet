﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour {

	private float currentHealth;                             // vie courante
	public Slider healthSlider;                              // Slider
	private bool intouchable;

	void Awake(){
		currentHealth = healthSlider.value; //initialisation de la valeur courante
		intouchable = false;
	}

	/**
	 * la bar de vie perd se que perd le player en fonction de la puissance de la balle reçu
	 */
	public void TakeDamage (int value)
	{
		if (!intouchable) {
			currentHealth -= value; //mise à jour de la valeur courante de la bar de vie

			//modification de la valeur sur le slider
			healthSlider.value = currentHealth;
		}
	}

	public float getMaxLife(){
		return healthSlider.maxValue;
	}

	public float getCurrentLife(){
		return this.currentHealth;
	}

	public void Intouchable(){
		intouchable = true;
	}

	public void noIntouchable(){
		intouchable = false;
	}

}
