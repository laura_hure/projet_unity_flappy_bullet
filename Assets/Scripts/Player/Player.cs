﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public float speed;
	private Vector3 leftBottomCameraBorder;
	private Vector3 rightTopCameraBorder;
	private string state;
	private bool action;
	private Vector2  targetPos;

	// Use this for initialization
	void Awake () {
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, 0));

		initTirPattern ();
	}
	
	// Update is called once per frame
	void Update () {

		if (action) { //si le joueur peut jouer
			
			//La methode de deplacement s'adapte en fonction du device
			#if  UNITY_EDITOR || UNITY_2017_STANDALONE || UNITY_WEBGL
				//sur PC on recupere les touches directionnelles
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (speed * Input.GetAxis ("Horizontal"), speed * Input.GetAxis ("Vertical"));
			
			//Sur telephone on recupere la position de l'ecran que l'utilisateur a touche
			#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_PHONE 
			
			//si le joueur a touche l'ecran
			if(Input.touchCount > 0){
				//enregistrement de la nouvelle position cible
				targetPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.touches[0].position);

				}
			
			//si la position du personne est différent de celle cible 
			if ((Vector2)transform.position != targetPos) {
				//permet de se déplacer progressivement vers la cible en suivant une trajectoire rectiligne
				transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
			}
			#endif

			//si le joueur depasse à droite on l'oblige à ne pas depasser l'ecran
			if (transform.position.x > rightTopCameraBorder.x) {
				transform.position = new Vector3 (rightTopCameraBorder.x, transform.position.y, 0);
			} else if (transform.position.y > rightTopCameraBorder.y) {
				transform.position = new Vector3 (transform.position.x, rightTopCameraBorder.y, 0);
			} else if (transform.position.x < leftBottomCameraBorder.x) {
				transform.position = new Vector3 (leftBottomCameraBorder.x, transform.position.y, 0);
			} else if (transform.position.y < leftBottomCameraBorder.y) {
				transform.position = new Vector3 (transform.position.x, leftBottomCameraBorder.y, 0);
			}
		}
		//lorsque la vie du joueur tombe à 0 il à perdu
		if (GetComponent<Life> ().getCurrentLife () <= 0) {
			GameState.Instance.loose ();
		}
	}
	
	//permet d'enlever d'empêcher les déplacements et tir du personnage et le rend intouchable par les balles
	public void stopAction(){
		Destroy (GetComponent<StraightBullet> ()); //enleve le pattern de tir
		GetComponent<Life>().Intouchable();
		action = false; // pas d'action pour le joueur
		GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, 0, 0); //pour eviter que le perso avance tout de meme
	}
	
	// permet au personnage de bouger et tirer et le rend touchable par les balles adverse.
	public void playAction(){
		initTirPattern ();
		action = true;
		GetComponent<Life> ().noIntouchable ();
	}
	
	//initialisation du pattern de tir
	private void initTirPattern(){
		StraightBullet straightBullet = gameObject.AddComponent<StraightBullet> (); //attribution d'un pattern de tir
		straightBullet.setFireTime(0.1f);
		straightBullet.setTypeOfBullet ("BulletPlayer");
		straightBullet.setPuissance (2);
		straightBullet.setMovement (new Vector2 (0, 5)); // les balles avancent tout droit de bas en haut
	}

	//convertie les radian en degrees
	private float RadToDeg(float angle){
		return (angle / 180) * Mathf.PI;
	}
}
