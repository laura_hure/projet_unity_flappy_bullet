﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchAction : MonoBehaviour {
	public float speedY;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//si l'utilisateur fait une action
		if (Input.anyKeyDown) {
			gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0,speedY);
		}
		
	}
}
