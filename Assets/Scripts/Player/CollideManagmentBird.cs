﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollideManagmentBird : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//si le joueur sort de l'écran par le haut ou le bas c'est le game over
		if(transform.position.y < Camera.main.ViewportToWorldPoint(new Vector3(0,0,0)).y ||
			transform.position.y > Camera.main.ViewportToWorldPoint(new Vector3(0,1,0)).y){
			GameState.Instance.loadScene();
		}
	}
	
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		//lorsque qu'il y a collision avec un pipe on joue une son, on lance l'animation de mort du player et on supprime le script 
		//qui permet au joueur d'interargir avec son personnage
		if (collider.gameObject.tag == "Pipe") {
			GameState.Instance.collidePipe (); 
			gameObject.AddComponent<EndAction> ();
			Destroy(GetComponent<TouchAction> ());
		}
		//si le joueur touche une piece il gagne 1 point
		if (collider.gameObject.tag == "goldPiece") {
			GameState.Instance.addScorePlayer (1);
		}
	}
}
