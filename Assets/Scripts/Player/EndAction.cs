﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndAction : MonoBehaviour {

	// Use this for initialization
	void Start () { 
	}
	
	// Update is called once per frame
	void Update () {
		//permet de faire tourner le personnage sur lui même lorsqu'il entre en contacte avec un pipe
		float rotation =gameObject.GetComponent<Rigidbody2D>().rotation;
		if (rotation < 180) {
			gameObject.GetComponent<Rigidbody2D> ().rotation = rotation + 10;
		}
	}
}
