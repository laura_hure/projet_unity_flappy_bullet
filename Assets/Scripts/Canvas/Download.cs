﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Download : MonoBehaviour {
	public float timeToGo;
	private bool isFadeIn;

	// Use this for initialization
	void Start () {
		isFadeIn = true;
	}

	// Update is called once per frame
	void Update () {
		//recuperation de la couleur du texte
		Color color = GameObject.FindGameObjectWithTag ("download").GetComponent<Text> ().color;

		//si on est visible on baisse la visibilite du texte
		if (isFadeIn) {
			color.a -= 0.01f;
		} else {//si non on l'augmente progressivement
			color.a += 0.01f;
		}

		//on donne la nouvelle couleur au texte
		GameObject.FindGameObjectWithTag ("download").GetComponent<Text>().color = color;

		//si le texte n'est plus visible isFadeIn passe a false
		if (color.a < 0) {
			isFadeIn = false;
		} else if (color.a > 1) {//si non passe à true
			isFadeIn = true;
		}

		Invoke ("go", timeToGo); //load la scene "Menu" après un certain temps
	}

	void go(){
		GameState.Instance.loadScene ();
	}
}