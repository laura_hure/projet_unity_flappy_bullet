﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarisaDownload : MonoBehaviour {
	private bool up;
	private float repeatTtime;
	private GameObject stars;

	// Use this for initialization
	void Start () {
		up = false; //au debut de l'animation on est ni en bas ni en haut
		repeatTtime =1f; //repete le changement de sens toutes les 2s
		stars  = GameObject.Find ("Stars"); //recuperation des étoiles

		InvokeRepeating("changeDirection",0.2f,repeatTtime);
	}
	
	// Update is called once per frame
	void Update () {
		if (!up) { //si on est pas en haut on monte
			transform.position = new Vector3 (transform.position.x, transform.position.y + 0.01f, transform.position.z);
		} else {// si non on descend
			transform.position = new Vector3 (transform.position.x, transform.position.y - 0.01f, transform.position.z);
		}
			
	}

	public void changeDirection(){
		up = !up;
	}
}
