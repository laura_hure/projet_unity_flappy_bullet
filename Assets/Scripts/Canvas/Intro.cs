﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour {
	private Text text;

	// Cinematique d'intro, affichage de message avec 3s d'écart entre chaque puis chargement de la scene suivante
	void Start () {
		text = GetComponent<Text> ();
		text.text = "La légende raconte, qu'un fabuleux trésor serait enfoui dans les ruines d'un ancien manoir.";

		Invoke ("state2", 3f);
		Invoke ("state3", 6f);
		Invoke ("loadNextScene", 9f);
	}
	
	public void state2(){
		text.text = "Marisa, une aventurière, est à la recherche de ce trésor.";
	}

	public void state3(){
		text.text = "Mais elle ne pouvait imaginer se qu'elle allait découvrir...";
	}

	public void loadNextScene(){
		GameState.Instance.loadScene ();
	}
}
