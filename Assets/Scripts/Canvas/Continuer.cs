﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Continuer : MonoBehaviour {
	private int isVisible; // 1 pour true et 0 pour false
	private int size; //taille du texte

	// si le joueur à sauegarder avant le boss on affiche le bouton si non
	// il est invisible
	void Start () {
		size = GetComponent<Text> ().fontSize;
		isVisible = GameState.Instance.getSaveIsBoss ();
		Color color = GetComponent<Text> ().color;

		if (isVisible == 1)
			color.a = 255;
		else
			color.a = 0;
		
		GetComponent<Text> ().color = color;
	}

	//au click sur le bouton VISIBLE on lance la combat du boss
	public void onClick(){
		if(isVisible == 1)
			GameState.Instance.startBossFight();
	}
}
