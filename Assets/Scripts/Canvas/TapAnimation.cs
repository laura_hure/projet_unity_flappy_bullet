﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapAnimation : MonoBehaviour {
	private float width;
	private float height;
	private bool grow;

	// Use this for initialization
	void Start () {
		width = transform.localScale.x;
		height = transform.localScale.y;
		grow = true;
	}
	
	// Update is called once per frame
	void Update () {

		//si la figure doit grossir on augmente la taille
		if (grow) {
			width += 0.005f;
			height += 0.005f;
		} else { //si non on diminue
			width -= 0.005f;
			height -= 0.005f;
		}

		//si la taille à depasser elle souhaite en grossissant alors on va retrecir la figure
		if (width > 1.2 && height > 1.2) {
			grow = false;
		} else if (width < 1 && height < 1) { //et inversement
			grow = true;
		}

		transform.localScale = new Vector2 (width, height); //attribution des nouvelles valeurs
	}
}
