﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickButton : MonoBehaviour {
	
	//action du boutton "commencer" dans le menu : lance la game
	public void onClick(){
		GameState.Instance.loadScene ();
	}
}
