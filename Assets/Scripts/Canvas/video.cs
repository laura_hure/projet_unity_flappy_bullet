﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class video : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		//si la video est prete 
		if (GetComponent<VideoPlayer> ().isPrepared) {
			//et qu'elle a fini
			if (!GetComponent<VideoPlayer> ().isPlaying) {
				GameState.Instance.startBossFight (); //on change de scene -> boss
			} 
		}
	}
}
