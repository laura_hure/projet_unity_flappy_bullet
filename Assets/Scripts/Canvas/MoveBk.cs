﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBk : MonoBehaviour {
	public Vector2 movement;

	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = movement; // les background avance de droite à gauche

		// If the backgound exits the screen
		// Set the X position with the original backGround3 X position
		if (transform.position.x < BackgroundManager.Instance.getCameraBorder().x - (BackgroundManager.Instance.getSize().x / 2))
		{
			transform.position = new Vector3(BackgroundManager.Instance.getPositionRestartX(),transform.position.y,5);
		}
	}
}
