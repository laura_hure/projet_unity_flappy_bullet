﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour {
	private static BackgroundManager _instance;
	public float positionRestartX;
	public string prefabName;
	Vector2 leaBoVomCameraBorder;
	float backgroundSizeX;
	float backgroundSizeY;

	public static BackgroundManager Instance{
		get{
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<BackgroundManager> ();

				if (_instance == null) {
					GameObject container = new GameObject ("Backgrounds");
					_instance = container.AddComponent<BackgroundManager> ();
				}
			}
			return _instance;
		}
	}

	// Use this for initialization
	void Start () {
		//Initialisation des 3 plans en background
		GameObject background1 = GameObject.FindGameObjectWithTag("background");

		backgroundSizeX = background1.GetComponent<SpriteRenderer> ().bounds.size.x;
		backgroundSizeY = background1.GetComponent<SpriteRenderer> ().bounds.size.y;

		GameObject background2 = Instantiate(
			Resources.Load (prefabName),
			new Vector3(
				background1.transform.position.x + backgroundSizeX, 
				background1.transform.position.y, 
				5),
			Quaternion.identity) as GameObject;
		
		GameObject background3 = Instantiate (
			                         Resources.Load (prefabName),
			                         new Vector3 (
				                         background2.transform.position.x + backgroundSizeX,
				                         background1.transform.position.y,
				                         5),
			                         Quaternion.identity) as GameObject;
		
		positionRestartX = background3.transform.position.x;

		leaBoVomCameraBorder = new Vector2 (Camera.main.ViewportToWorldPoint (new Vector3(0,0,0)).x, Camera.main.ViewportToWorldPoint (new Vector3(0,0,0)).y);
	}

	public Vector2 getCameraBorder(){
		return leaBoVomCameraBorder;
	}

	public float getPositionRestartX(){
		return positionRestartX;
	}

	public Vector2 getSize(){
		return new Vector2 (backgroundSizeX, backgroundSizeY);
	}
}
