﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	 //bords de la camera
	 private Vector3 leftTopCameraBorder;
	 private Vector3 leftBottomCameraBorder;
	 private Vector3 rightBottomCameraBorder;
	 protected int puissance; //puissance de la balle
	 private GameObject tireur; //tireur de la balle

	// Use this for initialization
	void Start () {
		//recuperation des bords de la camera
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, 0));

	}

	//----------------------------------GETTEUR ET SETTEUR---------------------
	public int getPuissance(){
		return this.puissance;
	}

	public void setPuissance(int puissance){
		this.puissance = puissance;
	}

	public void setTireur(GameObject tireur){
		this.tireur = tireur;
	}

	public GameObject getTireur(){
		return this.tireur;
	}

	public Vector2 getLeftTopCameraBorder(){
		return this.leftTopCameraBorder;
	}

	public Vector2 getLeftBottomCameraBorder(){
		return this.leftBottomCameraBorder;
	}

	public Vector2 getRightBottomCameraBorder(){
		return this.rightBottomCameraBorder;
	}

	//Toute balles possedent le meme comportement de base qui est de donner des damage a tout se qui peut en prendre
	void OnTriggerEnter2D(Collider2D collider)
	{
		//la balle ne blesse pas le tireur
		if (collider.gameObject != tireur) {
			//on ne peut toucher que se qui peut perdre de la vie
			if (collider.gameObject.GetComponent<Life> () != null) {
				collider.gameObject.GetComponent<Life> ().TakeDamage (this.puissance);
			}
		}
	}

	void OnParticleCollision(GameObject obj)
	{
		//la balle ne blesse pas le tireur
		if (obj != tireur) {
			//on ne peut toucher que se qui peut perdre de la vie
			if (obj.GetComponent<Life> () != null) {
				obj.GetComponent<Life> ().TakeDamage (this.puissance);

				//si l'objet à une animation de collision on la joue
				/*if (obj.GetComponent<Animator> ()) {
					obj.GetComponent<Animator> ().SetTrigger ("collision2");
				}*/
			}
		}
	}
}
