﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour {
	private Bullet parent;

	// Use this for initialization
	void Awake () {
		//recuperation du scripts contenant toutes les information minimal que doit avoir toutes les bullets du jeux
		parent = GetComponent<Bullet> ();
	}

	void Update(){
		//si on dépasse en haut ou en bas à droite ou à gauche la balle est détruite
		if (gameObject.transform.position.y > parent.getLeftTopCameraBorder().y 
			|| gameObject.transform.position.y < parent.getLeftBottomCameraBorder().y
			|| gameObject.transform.position.x < parent.getLeftTopCameraBorder().x 
			|| gameObject.transform.position.x > parent.getRightBottomCameraBorder().x) {

			destroy ();
		}
	}

	void destroy(){
		gameObject.SetActive (false);
	}
}
