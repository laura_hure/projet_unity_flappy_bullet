﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRebond : MonoBehaviour {
	private Bullet parent;
	private int angle;
	private float vitesse;

	void Awake(){
		//recuperation du comportement de base de toute balle
		parent = GetComponent<Bullet>();
	}

	// Update is called once per frame
	void Update () {
		//si on rencontre le bord gauche ou droit de l'écran
		if (gameObject.transform.position.x < parent.getLeftTopCameraBorder().x
			|| gameObject.transform.position.x > parent.getRightBottomCameraBorder().x) {

			//on fait rebondir la balle en attribuant l'inverse de l'angle 
			if (angle != 0) {
				angle = 180 - angle;
				gameObject.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (angle, vitesse);
			}
		}
		
	}

	//desactive la balle au bou de 5s
	void OnEnable(){
		//après 2s on invoke la methode de destruction
		Invoke ("destroy", 5f);
	}

	//desactivation de l'objet
	void destroy(){
		gameObject.SetActive (false);
	}
		
	//permet d'attribuer une vitesse a la balle
	public void setVitesse(float vitesse){
		this.vitesse = vitesse;
	}

	//permet d'attribuer un angle a la balle
	public void setAngle(int angle){
		this.angle = angle;
	}

	//convertie l'angle en degres en radian
	private float DegToRad(int angle){
		return (angle * Mathf.PI) / 180;
	}

	//return le vecteur de mouvement en fonction de l'angle et de la vitesse
	private Vector2 getBulletVelocity(int angle, float vitesse){
		return new Vector2(Mathf.Cos(DegToRad(angle))*vitesse,Mathf.Sin(DegToRad(angle))*vitesse);
	}
		
}
