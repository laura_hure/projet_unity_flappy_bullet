﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//pool generic
public class Pooler : MonoBehaviour {

	public static Pooler current;
	public List<GameObject> objets;
	public int pooledAmount = 20;
	public bool willGrow = true; //indique si la liste va s'adapter et grossir ou non

	public List<GameObject> pooledObjects;

	void Awake(){
		current = this; //initialisation de la variable static 
	}

	// Use this for initialization
	void Start () {
		//parcours de la liste de balles
		//initialisation du pool d'object au nombre souhaité pour chaque balle
		pooledObjects = new List<GameObject> ();
		for (int cpt = 0; cpt < objets.Count; cpt++) {
			//premier passage de 0 à 19, 2e de 20 à 40,... par exemple
			int min = pooledObjects.Count;
			int max = pooledAmount + pooledObjects.Count;
			for (int i = min; i < max; i++) {
				GameObject obj = (GameObject)Instantiate (objets [cpt]);
				obj.SetActive (false);
				pooledObjects.Add (obj); //tous les objets de la liste sont inactifs a l'initialisation
			}
		}
		
	}

	/**
	 * return le premier objets du pool qui n'est pas actif dans le jeux
	 */
	public GameObject GetPooledObject(string tag){
		for(int i=0; i<pooledObjects.Count; i++){
			if (!pooledObjects [i].activeInHierarchy && pooledObjects[i].tag == tag) {
				return pooledObjects [i]; //premier objet inactif trouvé
			}
		}

		//si le pool est autorisé a grossir, alors lorsqu'il n'y a plus d'object inactif disponible dans le pool on en cree un nouveau
		if (willGrow) {
			GameObject obj = (GameObject)Instantiate (Resources.Load(tag));
			obj.SetActive (false);
			pooledObjects.Add (obj);
			return obj;
		}

		return null; //si le pool ne peut pas grossir et qu'il n'y a pas d'object inactif on return null
	}
}
