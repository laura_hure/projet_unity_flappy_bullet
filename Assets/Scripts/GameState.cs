﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public sealed class GameState : MonoBehaviour {
	private static volatile GameState _instance;
	private int score;
	public string state;
	private bool isBoss;

	//defini le singleton, le lock est une securite pour empecher les acces concurrents
	public static GameState Instance{
		get{
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<GameState>();

				lock (_instance) {
					if (_instance == null) {
						GameObject container =  new GameObject("GameState");
						container.name = "GameState";
						_instance = container.AddComponent<GameState> ();
						DontDestroyOnLoad(container);
					}
				}
			}
			return _instance;
		}
	}

	void Awake(){
		//permet de rendre le singleton persistent
		lock (this) {
			if (_instance == null) {
				DontDestroyOnLoad (this.gameObject);
				_instance = this;

			} else {
				Destroy (gameObject);
			}
		}
	}

	void Start(){
		if (!PlayerPrefs.HasKey ("isBoss")) {
			isBoss = false;
		}
	}

	void Update(){
		//on load la scene à l'appui sur n'importe quel touche lorsque l'on est en game over
		if (Input.anyKeyDown) {
			if (SceneManager.GetActiveScene().name == "Scene4-GameOver" || SceneManager.GetActiveScene().name == "Scene6-GameOverVSBoss"
				|| SceneManager.GetActiveScene().name == "Scene8-Win") {
					loadScene ();
			}
		}
	}

	void FixedUpdate(){
		//on ne modifie le score que lorsque l'on est en state ou game over
		if (SceneManager.GetActiveScene().name == "scene3-Game" || SceneManager.GetActiveScene().name == "Scene4-GameOver") {
			GameObject.FindGameObjectWithTag ("Score").GetComponent<Text> ().text = "" + score;
		}
	}
	
	//augmente le score du joueur
	public void addScorePlayer(int toAdd){
		score += toAdd;
		SoundState.Instance.pointSound (); //un son est joue lorsqu'on gagne un point
		if (score == 5) { //a 5 point on loaad la scene suivante

			//en webGL on ne passe pas la cinématique et on lance directement le combat de boss
			#if UNITY_WEBGL
				startBossFight();
			#else 
				startIntroBoss ();
			#endif
		}
	}

	public int getScore(){
		return score;
	}
	
	//le son qui est joué lorsque le joueur touhce un pipe
	public void collidePipe(){
		SoundState.Instance.hitSound ();
	}

	public void startIntroBoss(){
		isBoss = true; // phase du boss atteinte
		save (); // sauvegarde du score et de la phase boss
		SoundState.Instance.stop (); //on arrête la musique de fond pour entendre la vidéo
		SceneManager.LoadScene ("Scene7-IntroBoss");
	}
		
	public void startGame(){
		state = "Game"; //on change de state lorsque l'on load la scene
		SoundState.Instance.startSound (); //son lors du start du jeux
		SoundState.Instance.setClip(4); //change la musique principale
		SceneManager.LoadScene ("Scene3-Game");
	}

	public void startMenu(){
		score = 0; // on remet le score a 0 en début de partie
		SceneManager.LoadScene ("Scene2-Menu"); //chargement de la scene suivante
	}
		
	public void startGameOver(){
		save (); // sauvegarde du score
		SoundState.Instance.deadSound ();
		SoundState.Instance.setClip (7);
		SceneManager.LoadScene ("Scene4-GameOver");
	}

	public void startIntro(){
		SoundState.Instance.setClip (5); //changement de la musique de fond
		SceneManager.LoadScene("Scene9-Intro"); //chargement de la cinématique d'introduction
	}

	public void startBossFight(){
		isBoss = true;
		save ();
		SoundState.Instance.setClip (6);
		SceneManager.LoadScene ("Scene5-Boss");
	}

	//en fonction de la scene où l'on se trouve on ne charge pas la même scene
	public void loadScene(){
		if (SceneManager.GetActiveScene ().name == "scene3-Game") {
			startGameOver ();
		} else if (SceneManager.GetActiveScene ().name == "Scene4-GameOver") {
			SoundState.Instance.setClip(8);
			startMenu ();
		} else if (SceneManager.GetActiveScene ().name == "Scene2-Menu") {
			startIntro ();
		} else if (SceneManager.GetActiveScene ().name == "Scene1-SplashScreen") {
			startMenu ();
		} else if (SceneManager.GetActiveScene ().name == "Scene9-Intro") {
			startGame ();
		}else if(SceneManager.GetActiveScene().name == "Scene6-GameOverVSBoss" ){
			SoundState.Instance.setClip(8);
			startMenu ();
		}else if (SceneManager.GetActiveScene().name == "Scene8-Win"){
			SoundState.Instance.setClip(8);
			startMenu ();
		}
	}

	//arret des activites du joueur
	public void NoPlayerAction(){
		GameObject.FindGameObjectWithTag("Player").GetComponent<Player> ().stopAction ();
	}

	//reprise des activites du joueur
	public void playerAction(){
		GameObject.Find("Player").GetComponent<Player> ().playAction ();
	}

	//le joueur a perdu on load la scene de game Over
	public void loose(){
		SoundState.Instance.setClip (7);
		SceneManager.LoadScene ("Scene6-GameOverVSBoss");
	}

	//le joueur à gagner
	public void win(){
		SoundState.Instance.setClip (7);
		SceneManager.LoadScene ("Scene8-Win");
	}

	//sauvegarde des données utilisateur
	public void save(){
		PlayerPrefs.SetInt ("score", score);
		if (isBoss)
			PlayerPrefs.SetInt ("isBoss", 1);
		else
			PlayerPrefs.SetInt ("isBoss", 0);
	}

	//si l'utilisateur à des scores de sauvegarde on l'affiche
	public int getSaveScore(){
		if(PlayerPrefs.HasKey("score")){
			return PlayerPrefs.GetInt("score");
		}
		return 0;
	}

	//indique si le joueur est a la phase du boss ou non
	public int getSaveIsBoss(){
		if(PlayerPrefs.HasKey("isBoss")){
			return PlayerPrefs.GetInt("isBoss");
		}
		return 0;
	}

	//suppression de la sauvegarde
	public void deleteSave(){
		PlayerPrefs.DeleteKey ("isBoss"); // on ne supprimer que cette information et on garde le score
	}
}
