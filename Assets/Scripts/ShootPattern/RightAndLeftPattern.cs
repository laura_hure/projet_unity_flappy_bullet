﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightAndLeftPattern : MonoBehaviour {
	private float fireTime;
	private float vitesse;
	private int cpt; //compteur pour savoir quand changer de sens
	private int sens;
	private int angle;
	private string[] typesOfBullet;
	private int puissance;

	// Use this for initialization
	void Start () {
		//initialisation des variables utiles
		sens = Random.Range(0,2); //1 pour droite, 0 pour gauche
		angle = Random.Range(-30,0);

		InvokeRepeating ("fire", fireTime, fireTime);
		InvokeRepeating ("fire", fireTime+0.1f, fireTime+0.1f);
		InvokeRepeating ("fire", fireTime+0.2f, fireTime+0.2f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//gauche 180+angle(+30,+60)
	//lance 3 jets de plusieurs balles
	private void fire(){
		if (cpt == 3) {
			//modification du sens
			sens = Random.Range(0,2);
			cpt = 0;
		}
			
			//-------------------------JET 1----------------------------------
		GameObject bullet = Pooler.current.GetPooledObject (typesOfBullet[cpt]); //recuperation de la balle
			bullet.transform.position = transform.position; //attribution de la position du tireur a la balle
		bullet.GetComponent<Bullet>().setPuissance(puissance+10); //puissance de la balle
			bullet.GetComponent<Bullet>().setTireur(gameObject); //on renseigne son tireur a la balle
			
			//au cas ou la balle ne soit pas une balle rebondissante
			if(bullet.GetComponent<BulletRebond>() != null){
				//modification de l'angle en fonction de la droite ou gauche
				if (sens == 1) //si droite
					bullet.GetComponent<BulletRebond> ().setAngle (angle); //donne l'angle a la balle
				else //si gauche
					bullet.GetComponent<BulletRebond>().setAngle(180-angle);
			
				bullet.GetComponent<BulletRebond> ().setVitesse (vitesse); //donne la vitesse a la balle
			}

			bullet.SetActive (true); //activation de la balle
			
		if (sens == 1) //modification en fonction  de la droite ou gauche
			bullet.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (angle, vitesse);
		else
			bullet.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (180 - angle, vitesse);

			//------------------------------------JET 2---------------------------------
		GameObject bullet2 = Pooler.current.GetPooledObject (typesOfBullet[cpt]); //recuperation de la balle
			bullet2.transform.position = transform.position; //attribution de la position du tireur a la balle
		bullet2.GetComponent<Bullet>().setPuissance(puissance+5); //puissance de la balle
			bullet2.GetComponent<Bullet>().setTireur(gameObject); //on renseigne son tireur a la balle
			
			if(bullet.GetComponent<BulletRebond>() != null){
				if (sens == 1)
					bullet2.GetComponent<BulletRebond> ().setAngle (angle-30); //donne l'angle a la balle
				else
					bullet2.GetComponent<BulletRebond> ().setAngle (180 - angle + 30);
			
				bullet2.GetComponent<BulletRebond> ().setVitesse (vitesse); //donne la vitesse a la balle
			}

			bullet2.SetActive (true); //activation de la balle
			
			if (sens == 1)
				bullet2.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (angle-30, vitesse);
			else
				bullet2.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (180 - angle + 30, vitesse);

			//------------------------------------JET 3---------------------------------
		GameObject bullet3 = Pooler.current.GetPooledObject (typesOfBullet[cpt]); //recuperation de la balle
			bullet3.transform.position = transform.position; //attribution de la position du tireur a la balle
			bullet3.GetComponent<Bullet>().setPuissance(puissance); //puissance de la balle
			bullet3.GetComponent<Bullet>().setTireur(gameObject); //on renseigne son tireur a la balle
			
			if(bullet3.GetComponent<BulletRebond>() != null){
				if (sens == 1)
					bullet3.GetComponent<BulletRebond> ().setAngle (angle -60); //donne l'angle a la balle
				else
					bullet3.GetComponent<BulletRebond> ().setAngle (180 - angle+60);
			
				bullet3.GetComponent<BulletRebond> ().setVitesse (vitesse); //donne la vitesse a la balle
			}

			bullet3.SetActive (true); //activation de la balle
			if (sens == 1)
				bullet3.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (angle - 60, vitesse);
			else
				bullet3.GetComponent<Rigidbody2D> ().velocity = getBulletVelocity (180 - angle + 60,vitesse);

		cpt++;//incrementation du conteur
	}

	//convertie l'angle en degres en radian
	private float DegToRad(int angle){
		return (angle * Mathf.PI) / 180;
	}

	//return le vecteur de mouvement en fonction de l'angle et de la vitesse
	private Vector2 getBulletVelocity(int angle, float vitesse){
		return new Vector2(Mathf.Cos(DegToRad(angle))*vitesse,Mathf.Sin(DegToRad(angle))*vitesse);
	}

	//-----------------------------SETTEUR------------------------
	public void setFireTime(float fireTime){
		this.fireTime = fireTime;
	}

	public void setVitesse(float vitesse){
		this.vitesse = vitesse;
	}

	public void setTypeOfBullets(string[] typeOfBullets){
		this.typesOfBullet = typeOfBullets;
	}

	public void setPuissance(int puissance){
		this.puissance = puissance;
	}
}
