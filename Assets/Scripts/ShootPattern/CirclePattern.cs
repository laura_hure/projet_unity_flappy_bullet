﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclePattern : MonoBehaviour {
	private float fireTime;
	private int nbBullets;
	private string typeOfBullet;
	private float vitesse;
	private int angle;
	private int puissance;

	// Use this for initialization
	void Start () {
		angle = RandomAngle(); //initialisation de l'angle aleatoire
		InvokeRepeating ("Fire", 0.5f, fireTime); //frequence de tir
		InvokeRepeating ("Fire", 0.5f, fireTime+0.1f); //2e cercle qui suit le premier
	}

	/**
	 * pattern pour tirer un cercle 
	 */
	private void Fire(){
		//pour chaque balles on decale l'angle de angle/nbBalles
		for (int i = 0; i < nbBullets; i++) {
			GameObject bullet = Pooler.current.GetPooledObject (typeOfBullet); //recuperation d'une balle disponible
			bullet.transform.position = transform.position; //la balle est tiré devant le tireur
			bullet.GetComponent<Bullet>().setPuissance(puissance);
			bullet.GetComponent<Bullet>().setTireur(gameObject); //on renseigne son tireur a la balle 
			bullet.SetActive(true); //activation de la balle
			bullet.GetComponent<Rigidbody2D>().velocity = getBulletVelocity(angle,vitesse); //on attribut l'angle et la vitesse a la balle
			angle += 360 / (nbBullets+1); //modification de l'angle pour la prochaine balle
		}
	}

	/**
	 * return un angle aleatoir entre 0 et 360
	 */
	private int RandomAngle(){
		return Random.Range (0, 360); //0 inclus, 360 exclu
	}

	//convertie l'angle en degres en radian
	private float DegToRad(int angle){
		return (angle * Mathf.PI) / 180;
	}

	//return le vecteur de mouvement en fonction de l'angle et de la vitesse
	private Vector2 getBulletVelocity(int angle, float vitesse){
		return new Vector2(Mathf.Cos(DegToRad(angle))*vitesse,Mathf.Sin(DegToRad(angle))*vitesse);
	}

	//--------------------SETTEUR------------------------
	public void setPuissance(int puissance){
		this.puissance = puissance;
	}

	public void setTypeOfBullet(string typeOfBullet){
		this.typeOfBullet = typeOfBullet;
	}

	public void setNbBullets(int nbBullets){
		this.nbBullets = nbBullets;
	}

	public void setFireTime(float fireTime){
		this.fireTime = fireTime;
	}

	public void setVitesse(float vitesse){
		this.vitesse = vitesse;
	}
}
