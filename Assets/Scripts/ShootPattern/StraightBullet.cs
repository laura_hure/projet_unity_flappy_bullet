﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightBullet : MonoBehaviour {
	private float fireTime;
	private Vector2 movement;
	private string typeOfBullet;
	private int puissance;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Fire", fireTime, fireTime);
	}

	// Update is called once per frame
	void Update () {
	}

	void Fire(){
		GameObject obj = Pooler.current.GetPooledObject(typeOfBullet);
		if (obj == null) return;
		obj.transform.position = transform.position; //on attribue la position du tireur a la ball
		obj.GetComponent<Bullet>().setPuissance(puissance); // attribution de la puissance a la balle
		obj.GetComponent<Bullet>().setTireur(gameObject); //on renseigne son tireur a la balle
		obj.SetActive(true); //activation de la balle
		obj.GetComponent<Rigidbody2D>().velocity = movement;
	}

	//----------------------SETTEUR-------------------------
	public void setFireTime(float fireTime){
		this.fireTime = fireTime;
	}

	public void setMovement(Vector2 movement){
		this.movement = movement;
	}

	public void setTypeOfBullet(string typeOfBullet){
		this.typeOfBullet = typeOfBullet;
	}

	public void setPuissance(int puissance){
		this.puissance = puissance;
	}
		
}
