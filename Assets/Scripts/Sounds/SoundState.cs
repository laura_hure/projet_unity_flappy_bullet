﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class SoundState : MonoBehaviour {
	private static volatile SoundState _instance;
	public string state;
	public List<AudioClip> sound; //liste des sons du jeux
	private bool isDo; //pour effectuer une action une seule fois
	private AudioSource audio;

	//definition du singleton
	public static SoundState Instance{
		get{
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<SoundState>();

				lock (_instance) {
					if (_instance == null) {
						GameObject container = new GameObject("SoundState");
						_instance = container.AddComponent<SoundState> ();
						DontDestroyOnLoad(container);
					}
				}
			}
			return _instance;
		}
	}

	void Awake(){
		//permet de rendre le singleton persistent
		lock (this) {
			if (_instance == null) {
				DontDestroyOnLoad (this.gameObject);
				_instance = this;
			} else {
				Destroy (gameObject);
			}
		}
	}

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void pointSound(){
		makeSound (sound [1]);
	}

	public void hitSound(){
		makeSound(sound[2]);
	}

	public void deadSound(){
		makeSound (sound [3]);
	}

	public void startSound(){
		makeSound (sound [0]);
	}

	//joue la musique
	private void makeSound(AudioClip originalClip){
		AudioSource.PlayClipAtPoint (originalClip, transform.position);
	}

	/**
	 * arrête la musique principale du jeu
	 */
	public void stop(){
		audio.Stop ();
	}
		

	/*
	 * permet de changer la musique principale du jeu
	 * */
	public void setClip(int indexe){
		audio.Stop ();
		audio.clip = sound[indexe]; 
		audio.Play ();
		audio.loop = true;
	}
}
