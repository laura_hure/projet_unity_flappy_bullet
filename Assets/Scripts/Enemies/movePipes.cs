﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour {
	public Vector2 move;
	private Transform pipeUpOriginalTransform, pipeDownOriginalTransform;
	private Vector3 leftBottomCameraBorder;
	private Vector3 leftTopCameraBorder;
	private Vector2 siz;

	// Use this for initialization
	void Start () {
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));
		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D> ().velocity = move; //les pipes avance tout le temps
		
		//lorsqie le pipe atteint le bord gauche de l'écran il est replacé tout à droite de la file de pipe
		if (gameObject.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) {
			gameObject.transform.position = new Vector3 (PipesManager.Instance.getPositionRestart (), getRandomY (), 0);
		}
	}

	private float getRandomY(){
		return Random.Range (leftBottomCameraBorder.y+2, leftTopCameraBorder.y-2);
	}
}
