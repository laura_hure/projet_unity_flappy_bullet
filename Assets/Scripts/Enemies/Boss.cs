﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour {
	private int state;
	private int nbState;
	private GameObject particle;
	private Life life;
	public Sprite[] imagesTransition;
	public Sprite[] sprites;
	private GameObject imageTransition;
	private Text textTransition;


	// Use this for initialization
	void Start () {
		nbState = 4; //definition du nombre d'etat du boss
		life= GetComponent<Life> (); //recuperation vie du boss
		state = 0;

		imageTransition = GameObject.Find ("FlandreTransitionImage");
		textTransition = GameObject.Find ("FlandreTransitionText").GetComponent<Text> ();
		imageTransition.SetActive (false);

		transition (0);
		state++;
	}
	
	// Update is called once per frame
	void Update () {
		//si le boss à perdu 1/nbState de sa vie
		if (state == 1 && life.getCurrentLife () < life.getMaxLife () - (life.getMaxLife () / nbState)) {
			particle.GetComponent<ParticleSystem> ().Stop(); //stop les particules
			transition (state); //transition
			state++; //passage a l'etat suivant
		} else if (state == 2 && life.getCurrentLife () < life.getMaxLife () - (life.getMaxLife () / nbState) * 2) {
			Destroy (GetComponent<CirclePattern> ()); //destruction du pattern précédent
			transition (state);
			state++;
		} else if (state == 3 && life.getCurrentLife () < life.getMaxLife () - (life.getMaxLife () / nbState) * 3) {
			transition (state);
			state++;
		} else if (life.getCurrentLife () <= 0) {
			transition (state);
			state++;
		}

		//lorsque la vie du boss tombe à 0 le joueur à gagné
		if (GetComponent<Life> ().getCurrentLife () <= 0) {
			GameState.Instance.win ();
		}
		
	}

	private void transition(int state){
		GameState.Instance.NoPlayerAction ();//desactivation des actions du joueur
	
		if (state == 0) {
			//changement de l'image et du texte puis affichage
			animation(0,"Je vais jouer avec toi !");

			Invoke("initParticle",2f); //pattern particle après 2 secondes d'attente
		} else if (state == 1) {
			animation (1,"Ha ha tu as esquivé, tu as esquivé !");
		
			StartCoroutine(initCirclePattern (0.5f,10,"BulletBossState1",5,2f,2f)); //patterne cercle
		} else if (state == 2) {
			animation (2, "C'est tout se que tu sait faire ?");
	
			StartCoroutine(initRebondPattern (new string[3]{ "BigBulletRebond", "MediumBulletRebond", "BulletRebond" },1f,5,3,2f));
		} else if (state == 3) {
			animation (3,"Cours pour ta vie !");

			StartCoroutine(initCirclePattern (1f, 10, "BulletBossState1", 5, 3f,2f));
		} else if (state == 4) {
			animation (4, "<pleures> Non Non NON !");

			Invoke ("death", 2); //mort du boss après 2s
		}
		Invoke("playerAction",2); //on rend la main au joueur
	}
	
	//initialisation du pattern circle
	private IEnumerator initCirclePattern(float fireTime, int nbBullets, string typeOfBullet, int puissance, float vitesse, float delay){
		yield return new WaitForSeconds(delay);//trop tot pour executer la method

		CirclePattern cirlePattern = gameObject.AddComponent<CirclePattern> (); //attribution d'un pattern de tir en cercle
		//parametrage du pattern
		cirlePattern.setFireTime (fireTime);
		cirlePattern.setNbBullets (nbBullets);
		cirlePattern.setTypeOfBullet (typeOfBullet);
		cirlePattern.setPuissance (puissance);
		cirlePattern.setVitesse (vitesse);
	}

	//initialisation pattern des balles qui rebondissent
	private IEnumerator initRebondPattern(string[] typeOfBullets, float fireTime, int puissance, float vitesse,float delay){
		yield return new WaitForSeconds(delay);

		RightAndLeftPattern rightAndLeftPattern = gameObject.AddComponent<RightAndLeftPattern> (); 
		rightAndLeftPattern.setTypeOfBullets (typeOfBullets);
		rightAndLeftPattern.setFireTime (fireTime);
		rightAndLeftPattern.setPuissance (puissance);
		rightAndLeftPattern.setVitesse (vitesse);
	}
	
	//initialisation des particule
	private void initParticle(){
		particle = GameObject.FindGameObjectWithTag ("BossParticles");
		particle.GetComponent<ParticleSystem> ().Play ();
		particle.GetComponent<Bullet> ().setPuissance (20);
		particle.GetComponent<Bullet> ().setTireur (gameObject);
	}
	
	//desactive la transition et on rend la main au joueur
	private void playerAction(){
		imageTransition.SetActive (false); //masque l'image (et le texte)
		GameState.Instance.playerAction ();
	}
	
	//lorsque le boss meurt on desactive tout les patterns
	private void death(){
		Destroy (GetComponent<CirclePattern> ());
		Destroy (GetComponent<RightAndLeftPattern> ());
		GameState.Instance.playerAction ();
	}
	
	//animation de la transition
	private void animation(int indexe, string message){
		//changement du sprite du boss
		GetComponent<SpriteRenderer> ().sprite = sprites [indexe];

		//changement de l'image et du texte puis affichage
		imageTransition.SetActive (true);

		//modification de la transparence pour rendre visible l'image
		Color color = imageTransition.GetComponent<Image> ().color;
		color.a = 255;
		imageTransition.GetComponent<Image> ().color = color;

		imageTransition.GetComponent<Image> ().sprite = imagesTransition [indexe]; 
		textTransition.text = message;
	}
}
