﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class PipesManager : MonoBehaviour {
	private static volatile PipesManager _instance;
	private Vector2 leftBottomCameraBorder;
	private Vector2 leftTopCameraBorder;
	private Vector2 rightBottomCameraBorder;
	Vector2 siz;
	private float positionRestartX;

	public static PipesManager Instance{
		get{
			if (_instance == null) {
				_instance = GameObject.FindGameObjectWithTag("PipeManager").GetComponent<PipesManager>();
				lock (_instance) {

					if (_instance == null) {
						GameObject container = GameObject.FindGameObjectWithTag("PipeManager");
						_instance = container.AddComponent<PipesManager> ();
					}
				}
			}
			return _instance;
		}
	}

	void Awake(){
	}

	// Use this for initialization
	void Start () {
		Debug.Log ("start pipes manager");
		//recuperation des bords de l'écran
		//Camera.main.WorldToViewportPoint / ViewportToWorldPoint
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));

		//initialisation des 3 pipes
		GameObject pipePaire1 = GameObject.FindGameObjectWithTag ("pipePair");

		//recuperation de la size des pipes
		siz.x = pipePaire1.GetComponent<SpriteRenderer> ().bounds.size.x;

		GameObject pipePaire2 = Instantiate (
			                        Resources.Load ("pipePaire"),
			                        new Vector3 (
										pipePaire1.transform.position.x + (siz.x/2) + 5,
				                        Random.Range (leftBottomCameraBorder.y+5, leftTopCameraBorder.y-5),
				                        0),
			                        Quaternion.identity) as GameObject;
		
		GameObject pipePaire3 = Instantiate (
			Resources.Load ("pipePaire"),
			new Vector3 (
				pipePaire2.transform.position.x + (siz.x/2) + 5,
				Random.Range (leftBottomCameraBorder.y+5, leftTopCameraBorder.y-5),
				0),
			Quaternion.identity) as GameObject;
		
		positionRestartX = pipePaire3.transform.position.x;
			
	}

	public float getPositionRestart(){
		return positionRestartX;
	}

	public Vector2 getPipeSize(){
		return siz;
	}

	public float getRandomY(){
		return Random.Range (leftBottomCameraBorder.y+5, leftTopCameraBorder.y-5);
	}

	public float getCameraBorder(){
		return leftBottomCameraBorder.x;
	}
}
