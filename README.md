# Projet UNITY pour l'IUT : base flappy bird et bullet hell

https://gitlab.com/laura_hure/projet_unity_flappy_bullet.git

## Important
Un bug connus concernant les vidéos stocker en local pour le webGL ma contraint à supprimer cette cinématique
pour le webGL (pas pour android). Je met donc ici le liens vers la vidéo pour ceux qui souhaiterais tout de même en prendre connaisssance : https://www.youtube.com/watch?v=wHA72ni92NE&t=10s

## Installation
L'application est valable sur support android et webGL uniquement, le reste n'ayant pas fait l'objet de test
ou d'adaptation.

Pour installer l'application android, récupérer le fichier flappyTouhou.apk dans le dossier build/buildAndroid.
Mettez-le dans vos dossier sur le téléphone et cliquez dessus pour lancer l'installation.

Pour lancer l'application WebGL, récupérer le contenu du dossier build/buildWeb et lancer le fichier index.html
dans firefox.

## Droit image et son

Je ne possède ni les droits ni les autorisations pour utiliser ces images. Je prierais donc les personnes
amené a accéder à ce projet de ne pas le diffuser, le commercialiser, ou en faire la diffusion de quelque
manière que se soit. Ce projet est à usage strictement personnel.

## Histoire

Dans ce jeux vous incarnez Marisa, une exploratrice avide d'aventures. Une légende raconte qu'un trésor serait enfoui dans un vieux manoir abandonné.
Marisa ne pouvais réfréner sa curiosité et la possibilité de découvrir des trésors oubliés. Elle partie donc à l'aventure...mais elle ne pouvait imaginer
ce qu'elle allait trouver...

## Principe du jeux

### Flappy

Le jeux commence comme un flappy bird standard. Il faut gagner des points en passant entre les deux tuyaux.
Sortir de l'écran en haut, en bas, ou toucher un tuyaux amène au gameOver. Il faut obtenir un total de 5 points pour enclencher une cinématique.

### Hell bullet

A l'issue de cette cinématique, le combat contre le boss va démarrer.Il s'agit maintenant d'un hell bullet. Votre personnage tire en continue droit devant lui.
Le boss à plusieurs phase délimiter par des transitions ou le boss communiquera avec le joueur.
Si ce dernier perd toutes sa vie un écran de game over s'affiche. Si non le joueur à gagner et un écran de victoire s"affiche.

Conseil : faite bien attention au point jaune sur marisa (votre personnage) c'est la zone vulnérable de son corps
elle ne doit pas toucher les balles adverse.

### Ecran de game over et de win

Dans un écran "GameOver" ou "Win" il est possible de retourner au menu du jeux en cliquant ou appuyant sur n'importe qu'elle touche du clavier.

### Menu

Dans le menu il est possible d'effectuer 2 actions, "commencer" (bouton flèche) ou "continuer".
Il n'est possible pour le joueur de continuer que lorsqu'il à atteint la cinématique précédent le boss du jeux.

### Sauvegarde auto
---

Une sauvegarde automatique s'effectue lorsque le joueur atteint la cinématique précédent le boss.
Lui permettant par la suite d'accéder directement au boss via le menu.

## Commandes

### Version PC

Pour la phase flappy le personnage avance tout seul et le joueur doit le maintenir dans les airs en tapant sur la barre espace.
Pour la phase hell bullet, le joueur peut se déplacer avec les flèches directionnelles.

### Version android

Pour la phase flappy, le personnage avance tout seul et le joueur doit le maintenir dans les airs en touchant l'écran.
Pour la phase du boss, le personnage se déplace vers le point de l'écran que le joueur à toucher. Toucher de nouveau l'écran pour changer sa trajectoire.
Le personnage s'arrête une fois qu'il à atteint le point toucher précédemment.

## Points techniques

- "GameState" et "SoundState" ne sont crée au splashScreen et utiliser tout le reste du jeux sans être recrée. Se sont des singleton persistant.

- pour des raisons évidente de performance, les balles du hell bullet sont gérer par un pool. Le pool se charge de créer les balles à la demande et fournie
les balles inactives qu'ils possèdent. Le pool peut gérer plusieurs types de balles, il suffit de préciser le type de balles que l'on souhaite récupérer au pool.

- Toute les balles particulières décende d'une même classe "bullet", ainsi dans les scripts de pattern de shoot, il est inutile de savoir quel type de balle est utilisé.
puisque toutes les balles respectent le minima suivant : une balle possède une puissance, un tireur, et fait des dommages à toute chose possédant une "Life" et qui n'est pas le tireur.
Ainsi les scripts de shoot doivent fournir ces informations a la balle.

- Un pattern de shoot peut être placé sur n'importe quel gameObject du moment que les paramètres minimum demandé sont fournis.

- Lors d'une transition du boss (d'une phase à une autre), le joueur ne peut plus bouger, ou tirer, et il ne peut plus prendre de dégats.

- Le personnage du joueur ne peut pas sortir de l'écran lors du combat contre le boss.

- Les déplacements du personnage sont différents selon le device. Pour savoir qu'elle méthode utilisé en fonction du device une synthaxe particuliète à été
utilisé pour détecté le type de device (voir tuto : https://unity3d.com/fr/learn/tutorials/projects/2d-roguelike-tutorial/adding-mobile-controls).

## Bug connus

- La vidéo cinématique ne se lance pas en webGL.
